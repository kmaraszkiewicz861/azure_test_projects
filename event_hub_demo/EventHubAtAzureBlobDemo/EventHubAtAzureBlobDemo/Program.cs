﻿using System;
using System.Text;
using System.Threading.Tasks;
using Azure.Messaging.EventHubs;
using Azure.Messaging.EventHubs.Consumer;
using Azure.Storage.Blobs;

namespace EventHubAtAzureBlobDemo
{
    class Program
    {
        private const string ehubNamespaceConnectionString = "Endpoint=sb://azureblobstorageeventhubdemo.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=s2i/e3bEZzeYQEVfO6bgrFmkjuU7q/ZSUBBo9Hw+ySo=";
        private const string eventHubName = "azureblobstorageeventhub";
        private const string blobStorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=azureblobdemo1234;AccountKey=NGBXvOCvpnSGplX8SzjpQdCxHTrQuSNrkDuo+A/MzPD7vnRZTFmLB3aq8909uRzzjeFtd9ypsr6d+AStiG3E8A==;EndpointSuffix=core.windows.net";
        private const string blobContainerName = "azureblobdemo1234";

        static BlobContainerClient storageClient;
        static EventProcessorClient processor;

        static async Task Main(string[] args)
        {
            string consumerGroup = EventHubConsumerClient.DefaultConsumerGroupName;

            storageClient = new BlobContainerClient(blobStorageConnectionString, blobContainerName);
            processor = new EventProcessorClient(storageClient, consumerGroup, ehubNamespaceConnectionString, eventHubName);

            processor.ProcessErrorAsync += Processor_ProcessErrorAsync;
            processor.ProcessEventAsync += Processor_ProcessEventAsync;

            Console.WriteLine("Waiting for the events");
            await processor.StartProcessingAsync();
            await Task.Delay(TimeSpan.FromSeconds(30));
            Console.WriteLine("Exiting app...");
            await processor.StopProcessingAsync();
        }

        private static async Task Processor_ProcessEventAsync(Azure.Messaging.EventHubs.Processor.ProcessEventArgs arg)
        {
            Console.WriteLine($"\tReceved event {Encoding.UTF8.GetString(arg.Data.EventBody.ToArray())}");

            await arg.UpdateCheckpointAsync(arg.CancellationToken);
        }

        private static Task Processor_ProcessErrorAsync(Azure.Messaging.EventHubs.Processor.ProcessErrorEventArgs arg)
        {
            // Write details about the error to the console window
            Console.WriteLine($"\tPartition '{ arg.PartitionId}': an unhandled exception was encountered. This was not expected to happen.");
            Console.WriteLine(arg.Exception.Message);
            return Task.CompletedTask;
        }
    }
}
