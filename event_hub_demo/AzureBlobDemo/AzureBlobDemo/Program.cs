﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Azure.Storage.Blobs;

namespace AzureBlobDemo
{
    class Program
    {
        public static string connectionString = "DefaultEndpointsProtocol=https;AccountName=azureblobdemo1234;AccountKey=NGBXvOCvpnSGplX8SzjpQdCxHTrQuSNrkDuo+A/MzPD7vnRZTFmLB3aq8909uRzzjeFtd9ypsr6d+AStiG3E8A==;EndpointSuffix=core.windows.net";
        public static string blobContainerName = "azureblobdemo1234";

        static async Task Main(string[] args)
        {
            var client = new BlobServiceClient(connectionString);
            
            try
            {
                await client.CreateBlobContainerAsync(blobContainerName);
            }
            catch
            {

            }

            var testJson = "{'key': 'value'}";

            var container = new BlobContainerClient(connectionString, blobContainerName);

            var containerClient = container.GetBlobClient($"testJson{Guid.NewGuid()}.json");

            using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(testJson)))
            {
                await containerClient.UploadAsync(ms);
            }
        }
    }
}
